/* -*- Mode: C; c-basic-offset: 2; indent-tabs-mode: nil -*-
 *
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#define _XOPEN_SOURCE 600
#define _GNU_SOURCE
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#ifdef __linux
#include <sys/prctl.h>
#endif

#include "metaterm-rootshell.h"

#include <glib/gi18n.h>
#include <gio/gunixfdlist.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>

static const gchar *global_argv0;

typedef struct {
  GMainLoop *loop;
  GDBusConnection *app;

  GHashTable *shell_pids;
} ShellHelper;

typedef struct {
  ShellHelper *helper;
  const char *ptyname;
} ShellHelperData;

/* Lifted from vte/src/pty.c; we can't use vte_pty_child_setup
 * since it expects a VtePty structure.
 */
/* Reset the handlers for all known signals to their defaults.  The parent
 * (or one of the libraries it links to) may have changed one to be ignored. */
static void
_vte_pty_reset_signal_handlers(void)
{
#ifdef SIGHUP
	signal(SIGHUP,  SIG_DFL);
#endif
	signal(SIGINT,  SIG_DFL);
	signal(SIGILL,  SIG_DFL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGFPE,  SIG_DFL);
#ifdef SIGKILL
	signal(SIGKILL, SIG_DFL);
#endif
	signal(SIGSEGV, SIG_DFL);
#ifdef SIGPIPE
	signal(SIGPIPE, SIG_DFL);
#endif
#ifdef SIGALRM
	signal(SIGALRM, SIG_DFL);
#endif
	signal(SIGTERM, SIG_DFL);
#ifdef SIGCHLD
	signal(SIGCHLD, SIG_DFL);
#endif
#ifdef SIGCONT
	signal(SIGCONT, SIG_DFL);
#endif
#ifdef SIGSTOP
	signal(SIGSTOP, SIG_DFL);
#endif
#ifdef SIGTSTP
	signal(SIGTSTP, SIG_DFL);
#endif
#ifdef SIGTTIN
	signal(SIGTTIN, SIG_DFL);
#endif
#ifdef SIGTTOU
	signal(SIGTTOU, SIG_DFL);
#endif
#ifdef SIGBUS
	signal(SIGBUS,  SIG_DFL);
#endif
#ifdef SIGPOLL
	signal(SIGPOLL, SIG_DFL);
#endif
#ifdef SIGPROF
	signal(SIGPROF, SIG_DFL);
#endif
#ifdef SIGSYS
	signal(SIGSYS,  SIG_DFL);
#endif
#ifdef SIGTRAP
	signal(SIGTRAP, SIG_DFL);
#endif
#ifdef SIGURG
	signal(SIGURG,  SIG_DFL);
#endif
#ifdef SIGVTALARM
	signal(SIGVTALARM, SIG_DFL);
#endif
#ifdef SIGXCPU
	signal(SIGXCPU, SIG_DFL);
#endif
#ifdef SIGXFSZ
	signal(SIGXFSZ, SIG_DFL);
#endif
#ifdef SIGIOT
	signal(SIGIOT,  SIG_DFL);
#endif
#ifdef SIGEMT
	signal(SIGEMT,  SIG_DFL);
#endif
#ifdef SIGSTKFLT
	signal(SIGSTKFLT, SIG_DFL);
#endif
#ifdef SIGIO
	signal(SIGIO,   SIG_DFL);
#endif
#ifdef SIGCLD
	signal(SIGCLD,  SIG_DFL);
#endif
#ifdef SIGPWR
	signal(SIGPWR,  SIG_DFL);
#endif
#ifdef SIGINFO
	signal(SIGINFO, SIG_DFL);
#endif
#ifdef SIGLOST
	signal(SIGLOST, SIG_DFL);
#endif
#ifdef SIGWINCH
	signal(SIGWINCH, SIG_DFL);
#endif
#ifdef SIGUNUSED
	signal(SIGUNUSED, SIG_DFL);
#endif
}

static void
shell_helper_child_setup (gpointer datap)
{
  ShellHelperData *data = datap;
  int fd;

  fd = open (data->ptyname, O_RDWR);
  if (fd == -1)
    _exit (127);

  setsid ();
  setpgid (0, 0);
  
#ifdef TIOCSCTTY
  /* TIOCSCTTY is defined?  Let's try that, too. */
  ioctl(fd, TIOCSCTTY, fd);
#endif

  if (fd != 0)
    dup2 (fd, 0);
  if (fd != 1)
    dup2 (fd, 1);
  if (fd != 2)
    dup2 (fd, 2);

  if (fd > 2)
    close (fd);

  _vte_pty_reset_signal_handlers ();

  setenv ("TERM", "xterm", TRUE);
}

static gboolean
spawn_shell (ShellHelper  *helper,
             const char   *cwd,
             const char   *ptyname,
             GPid         *out_pid,
             GError      **error)
{
  char *shell_argv[] = { "/bin/sh", NULL };
  ShellHelperData helper_data;

  helper_data.helper = helper;
  helper_data.ptyname = ptyname;

  return g_spawn_async (cwd, shell_argv, NULL, G_SPAWN_DO_NOT_REAP_CHILD,
                        shell_helper_child_setup, &helper_data, out_pid, error);
}

static void
on_shell_exited (GPid     pid,
                 int      status,
                 gpointer data)
{
  ShellHelper *self = data;

  g_dbus_connection_emit_signal (self->app,
                                 "org.verbum.Metaterm.Rootshell",
                                 "/org/verbum/Metaterm/Rootshell",
                                 "org.verbum.Metaterm.RootShell",
                                 "ChildExited",
                                 g_variant_new ("(xi)", (gint64) pid, status),
                                 NULL);
}

static GDBusMessage *
shell_helper_message_filter (GDBusConnection *connection,
                             GDBusMessage    *message,
                             gboolean         incoming,
                             gpointer         user_data)
{
  ShellHelper *self = user_data;

  if (!incoming)
    return message;

  g_assert (self->app == connection);
  
  g_debug ("in message filter");

  if (strcmp (g_dbus_message_get_member (message), "RequestShell") == 0
      && strcmp (g_dbus_message_get_signature (message), "ss") == 0)
    {
      GError *error = NULL;
      GDBusMessage *reply;
      const char *cwd;
      const char *ptsname;
      GPid pid;

      g_debug ("handling RequestShell");

      g_variant_get (g_dbus_message_get_body (message),
                     "(ss)",
                     &cwd,
                     &ptsname);

      if (!spawn_shell (self, cwd, ptsname, &pid, &error))
        {
          reply = g_dbus_message_new_method_error (message,
                                                   "org.verbum.Metaterm.ShellSpawnError",
                                                   "%s",
                                                   error->message);
          g_clear_error (&error);
        }
      else
        {
          g_child_watch_add (pid, on_shell_exited, self);

          reply = g_dbus_message_new_method_reply (message);
          g_dbus_message_set_body (reply, g_variant_new ("(x)", (gint64)pid));
        }
      
      if (!g_dbus_connection_send_message (connection, reply, 0, NULL, &error))
        g_error ("%s", error->message);
      g_object_unref (reply);

      g_debug ("finished handling RequestShell");

      return NULL;
    }
  else
    g_warning ("unmatched message: %s %s", g_dbus_message_get_member (message),
               g_dbus_message_get_signature (message));

  return message;
}

static int
shell_helper_main (int    argc,
                   char **argv)
{
  ShellHelper *self;
  GError *error = NULL;
  const char *address;

  g_debug ("in shell helper");

  g_assert (argc >= 3);
  address = argv[2];

  g_set_prgname ("metaterm-rootshell-helper");

  if (getuid () != 0)
    {
      g_printerr ("%s", "Not running as root?");
      exit (1);
    }

#ifdef __linux
  prctl (PR_SET_PDEATHSIG, SIGTERM);
#endif  

  self = g_slice_new0 (ShellHelper);

  self->loop = g_main_loop_new (NULL, FALSE);

  g_debug ("address is %s", address);

  self->app = g_dbus_connection_new_for_address_sync (address,
                                                      G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT,
                                                      NULL,
                                                      NULL,
                                                      &error);
  if (self->app == NULL)
    g_error ("%s", error->message);
  g_dbus_connection_add_filter (self->app,
                                shell_helper_message_filter,
                                self,
                                NULL);
  g_debug ("running mainloop");
  g_main_loop_run (self->loop);

  g_debug ("shell helper exiting");

  g_object_unref (self->app);
  g_main_loop_unref (self->loop);
  g_slice_free (ShellHelper, self);

  return 0;
}

/*
 * _vte_pty_ptsname:
 * @master: file descriptor to the PTY master
 * @error: a location to store a #GError, or %NULL
 *
 * Returns: a newly allocated string containing the file name of the
 *   PTY slave device, or %NULL on failure with @error filled in
 */
static char *
_vte_pty_ptsname(int master,
                 GError **error)
{
#if defined(HAVE_PTSNAME_R)
	gsize len = 1024;
	char *buf = NULL;
	int i, errsv;
	do {
		buf = g_malloc0(len);
		i = ptsname_r(master, buf, len - 1);
		switch (i) {
		case 0:
			/* Return the allocated buffer with the name in it. */
			return buf;
			break;
		default:
                        errsv = errno;
			g_free(buf);
                        errno = errsv;
			buf = NULL;
			break;
		}
		len *= 2;
	} while ((i != 0) && (errno == ERANGE));

        g_set_error(error, VTE_PTY_ERROR, VTE_PTY_ERROR_PTY98_FAILED,
                    "%s failed: %s", "ptsname_r", g_strerror(errno));
        return NULL;
#elif defined(HAVE_PTSNAME)
	char *p;
	if ((p = ptsname(master)) != NULL) {
		_vte_debug_print(VTE_DEBUG_PTY, "PTY slave is `%s'.\n", p);
		return g_strdup(p);
	}

        g_set_error(error, VTE_PTY_ERROR, VTE_PTY_ERROR_PTY98_FAILED,
                    "%s failed: %s", "ptsname", g_strerror(errno));
        return NULL;
#elif defined(TIOCGPTN)
	int pty = 0;
	if (ioctl(master, TIOCGPTN, &pty) == 0) {
		_vte_debug_print(VTE_DEBUG_PTY,
				"PTY slave is `/dev/pts/%d'.\n", pty);
		return g_strdup_printf("/dev/pts/%d", pty);
	}

        g_set_error(error, VTE_PTY_ERROR, VTE_PTY_ERROR_PTY98_FAILED,
                    "%s failed: %s", "ioctl(TIOCGPTN)", g_strerror(errno));
        return NULL;
#else
#error no ptsname implementation for this platform
#endif
}

struct _MetatermRootshellPrivate
{
  GPid shell_helper_pid;
  GDBusServer *server;
  GDBusConnection *shell_helper;
  char *guid;
  GHashTable *serial_to_tab;
  GList *tabs;
};

typedef struct
{
  MetatermRootshell *app;
  GPid shell_pid;
  VteTerminal *term;
  VtePty *pty;
  enum {
    TAB_STATE_UNINIT      = 0,
    TAB_STATE_WAIT_HELPER = 1,
    TAB_STATE_WAIT_SHELL = 2,
    TAB_STATE_RUNNING = 3
  } state;
} MetatermRootshellTab;

G_DEFINE_TYPE (MetatermRootshell, metaterm_rootshell, METATERM_TYPE_APP);

static void
metaterm_rootshell_init (MetatermRootshell *self)
{
  self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, METATERM_TYPE_ROOTSHELL,
					    MetatermRootshellPrivate);
  self->priv->serial_to_tab = g_hash_table_new (NULL, NULL);
}

static void
metaterm_rootshell_process_tab_shell_message (MetatermRootshell    *self,
                                              MetatermRootshellTab *tab,
                                              GDBusMessage         *reply)
{
  gint64 pidx;

  g_debug ("processing tab shell message serial %lu", (gulong) g_dbus_message_get_reply_serial (reply));

  g_variant_get (g_dbus_message_get_body (reply), "(x)", &pidx);

  tab->shell_pid = (GPid)pidx;
  tab->state = TAB_STATE_RUNNING;
}

static GDBusMessage *
rootshell_message_filter (GDBusConnection *connection,
                          GDBusMessage    *message,
                          gboolean         incoming,
                          gpointer         user_data)
{
  MetatermRootshell *self = user_data;
  (void)self;

  if (!incoming)
    return message;

  if (g_dbus_message_get_message_type (message) == G_DBUS_MESSAGE_TYPE_METHOD_RETURN)
    {
      guint32 serial;
      MetatermRootshellTab *tab;

      serial = g_dbus_message_get_reply_serial (message);
      if ((tab = g_hash_table_lookup (self->priv->serial_to_tab, GUINT_TO_POINTER (serial))) != NULL)
        {
          metaterm_rootshell_process_tab_shell_message (self, tab, message);
          g_hash_table_remove (self->priv->serial_to_tab, GUINT_TO_POINTER (serial));
        }
      else
        g_warning ("unmatched serial %lu", (gulong) serial);
      
      return NULL;
    }

  return message;
}

static void
metaterm_rootshell_tab_request_shell (MetatermRootshell *self,
                                      MetatermRootshellTab *tab)
{
  GError *error = NULL;
  guint32 serial;
  GDBusMessage *request;
  int master_fd;
  char *ptsname;
  
  g_assert (tab->state == TAB_STATE_UNINIT || tab->state == TAB_STATE_WAIT_HELPER);

  request = g_dbus_message_new_method_call ("org.verbum.Metaterm.Rootshell",
                                            "/org/verbum/Metaterm/Rootshell",
                                            "org.verbum.Metaterm.RootShell",
                                            "RequestShell");

  master_fd = vte_pty_get_fd (tab->pty);
  ptsname = _vte_pty_ptsname (master_fd, NULL);

  g_dbus_message_set_body (request, g_variant_new ("(ss)", "/", ptsname));
  if (!g_dbus_connection_send_message (self->priv->shell_helper,
                                       request, 0,
                                       &serial, &error))
    g_error ("%s", error->message);
  g_free (ptsname);
  g_object_unref (request);
  g_hash_table_insert (self->priv->serial_to_tab, GUINT_TO_POINTER(serial), tab);
  tab->state = TAB_STATE_WAIT_SHELL;
  g_debug ("tab %p serial %lu waiting for shell", tab, (gulong)serial);
}

static gboolean
on_new_dbus_connection (GDBusServer *server,
                        GDBusConnection *connection,
                        gpointer user_data)
{
  MetatermRootshell *self = user_data;
  GCredentials *credentials;
  uid_t uid;
  GList *iter;

  if (self->priv->shell_helper != NULL)
    {
      g_warning ("duplicate connection");
      return FALSE;
    }

  credentials = g_dbus_connection_get_peer_credentials (connection);
  if (credentials == NULL)
    return FALSE;
  uid = g_credentials_get_unix_user (credentials, NULL);
  if (uid != 0)
    {
      g_warning ("got unexpected uid %lu", (gulong)uid);
      return FALSE;
    }

  g_debug ("got connection from helper");
  self->priv->shell_helper = g_object_ref (connection);
  g_dbus_connection_add_filter (connection,
                                rootshell_message_filter,
                                self,
                                NULL);
  
  for (iter = self->priv->tabs; iter; iter = iter->next)
    {
      MetatermRootshellTab *tab = iter->data;

      g_assert (tab->state == TAB_STATE_WAIT_HELPER);
      
      metaterm_rootshell_tab_request_shell (self, tab);
    }

  return TRUE;
}

static void
metaterm_rootshell_ensure_shell_helper (MetatermRootshell *self)
{
  GError *error = NULL;
  char *child_argv[5];
  int i;
  const char *address;
  GPid pid;

  if (self->priv->shell_helper_pid != 0)
    return;

  g_debug ("spawning shell helper");

  self->priv->guid = g_dbus_generate_guid ();
  self->priv->server = g_dbus_server_new_sync ("unix:tmpdir=/tmp", 0,
                                               self->priv->guid,
                                               NULL, NULL, &error);
  if (self->priv->server == NULL)
    {
      g_printerr ("Failed to create DBus server: %s\n", error->message);
      exit (1);
    }

  g_dbus_server_start (self->priv->server);

  g_signal_connect (self->priv->server,
                    "new-connection",
                    G_CALLBACK (on_new_dbus_connection),
                    self);

  address = g_dbus_server_get_client_address (self->priv->server);

  g_debug ("server address: %s", address);
  
  i = 0;
  child_argv[i++] = "pkexec";
  child_argv[i++] = (char*)global_argv0;
  child_argv[i++] = "--helper";
  child_argv[i++] = (char*)address;
  child_argv[i++] = NULL;
  g_assert (i == G_N_ELEMENTS (child_argv)); 

  if (!g_spawn_async_with_pipes (NULL,
                                 (gchar**)child_argv,
                                 NULL,
                                 G_SPAWN_SEARCH_PATH | G_SPAWN_DO_NOT_REAP_CHILD,
                                 NULL,
                                 NULL,
                                 &pid,
                                 NULL,
                                 NULL,
                                 NULL,
                                 &error))
    {
      g_error ("failed to execute pkexec '%s': %s",
               global_argv0, error->message);
    }
  self->priv->shell_helper_pid = pid;

  g_debug ("shell helper pid: %ld", (long) self->priv->shell_helper_pid);
}

static void
on_tab_destroy (GtkWidget *widget,
                gpointer   user_data)
{
  MetatermRootshellTab *tab = user_data;

  tab->app->priv->tabs = g_list_remove (tab->app->priv->tabs, tab);

  g_object_unref (tab->pty);
  g_object_unref (tab->app);
  g_free (tab);
}

static void
metaterm_rootshell_new_tab (MetatermApp *app,
                            GtkLabel    *label,
                            VteTerminal *term)
{
  MetatermRootshell *self = METATERM_ROOTSHELL (app);
  MetatermRootshellTab *tab;
  GError *error = NULL;

  tab = g_new0 (MetatermRootshellTab, 1);
  tab->app = g_object_ref (self);
  tab->term = term;
  
  metaterm_rootshell_ensure_shell_helper (self);

  tab->pty = vte_pty_new (VTE_PTY_NO_HELPER, &error);
  if (!tab->pty)
    g_error ("%s", error->message);

  vte_terminal_set_pty_object (tab->term, tab->pty);

  if (self->priv->shell_helper == NULL)
    tab->state = TAB_STATE_WAIT_HELPER;
  else
    metaterm_rootshell_tab_request_shell (self, tab);
  
  g_signal_connect (term, "destroy", G_CALLBACK (on_tab_destroy), tab);
  
  self->priv->tabs = g_list_prepend (self->priv->tabs, tab);
}

static void
metaterm_rootshell_finalize (GObject *object)
{
  MetatermRootshell *self = METATERM_ROOTSHELL (object);

  (* G_OBJECT_CLASS (metaterm_rootshell_parent_class)->finalize) (G_OBJECT (self));
}

static void
metaterm_rootshell_class_init (MetatermRootshellClass *class)
{
  GObjectClass *gobject_class;
  MetatermAppClass *app_class;

  gobject_class = G_OBJECT_CLASS (class);
  app_class = METATERM_APP_CLASS (class);

  gobject_class->finalize = metaterm_rootshell_finalize;

  app_class->new_tab = metaterm_rootshell_new_tab;

  g_type_class_add_private (class, sizeof (MetatermRootshellPrivate));
}

static int
primary_app_main (int    argc,
                  char **argv)
{
  MetatermRootshell *app;

  g_set_prgname ("metaterm-rootshell");
      
  app = g_object_new (METATERM_TYPE_ROOTSHELL,
                      "application-id", "org.verbum.metaterm.RootShell",
                      "flags", 0,
                      "title", _("Root Shell"),
                      NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}

int
main (int    argc,
      char **argv)
{
  global_argv0 = argv[0];

  g_type_init ();

  if (argc == 3 && strcmp (argv[1], "--helper") == 0)
    return shell_helper_main (argc, argv);
  else
    return primary_app_main (argc, argv);
}
