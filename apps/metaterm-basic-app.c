/* -*- Mode: C; c-basic-offset: 2; indent-tabs-mode: nil -*-
 *
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "metaterm-basic-app.h"

#include <glib/gi18n.h>


struct _MetatermBasicAppPrivate
{
  gpointer unused;
};

G_DEFINE_TYPE (MetatermBasicApp, metaterm_basic_app, METATERM_TYPE_APP);

static void
metaterm_basic_app_init (MetatermBasicApp *self)
{
  self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, METATERM_TYPE_BASIC_APP,
					    MetatermBasicAppPrivate);
}

static void
metaterm_basic_app_finalize (GObject *object)
{
  MetatermBasicApp *self = METATERM_BASIC_APP (object);

  (* G_OBJECT_CLASS (metaterm_basic_app_parent_class)->finalize) (G_OBJECT (self));
}

static void
metaterm_basic_app_class_init (MetatermBasicAppClass *class)
{
  GObjectClass *gobject_class;
  MetatermAppClass *app_class;

  gobject_class = G_OBJECT_CLASS (class);
  app_class = METATERM_APP_CLASS (class);
  (void)app_class;

  gobject_class->finalize = metaterm_basic_app_finalize;

  g_type_class_add_private (class, sizeof (MetatermBasicAppPrivate));
}

int
main (int    argc,
      char **argv)
{
  MetatermBasicApp *app;
  g_type_init ();

  g_set_prgname ("metaterm-basic-app");
      
  app = g_object_new (METATERM_TYPE_BASIC_APP,
                      "application-id", "org.verbum.metaterm.BasicApp",
                      "flags", 0,
                      "title", _("Metaterm Basic App"),
                      NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
