/* 
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __METATERM_BASIC_APP_H__
#define __METATERM_BASIC_APP_H__

#include "libmetaterm.h"

G_BEGIN_DECLS

#define METATERM_TYPE_BASIC_APP              (metaterm_basic_app_get_type ())
#define METATERM_BASIC_APP(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), METATERM_TYPE_BASIC_APP, MetatermBasicApp))
#define METATERM_BASIC_APP_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), METATERM_TYPE_BASIC_APP, MetatermBasicAppClass))
#define METATERM_IS_BASIC_APP(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), METATERM_TYPE_BASIC_APP))
#define METATERM_IS_BASIC_APP_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), METATERM_TYPE_BASIC_APP))
#define METATERM_BASIC_APP_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), METATERM_TYPE_BASIC_APP, MetatermBasicAppClass))

typedef struct _MetatermBasicApp         MetatermBasicApp;
typedef struct _MetatermBasicAppClass    MetatermBasicAppClass;
typedef struct _MetatermBasicAppPrivate  MetatermBasicAppPrivate;

struct _MetatermBasicApp
{
  MetatermApp parent;

  /*< private >*/
  MetatermBasicAppPrivate *priv;
};

struct _MetatermBasicAppClass
{
  MetatermAppClass parent;
};

GType metaterm_basic_app_get_type (void);

G_END_DECLS

#endif
