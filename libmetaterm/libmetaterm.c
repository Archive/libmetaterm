/* -*- Mode: C; c-basic-offset: 2; indent-tabs-mode: nil -*-
 *
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "libmetaterm.h"

#include <glib/gi18n.h>

enum
{
  PROP_NONE,
  PROP_TITLE
};

struct _MetatermAppPrivate
{
  GtkActionGroup *actions;
  GtkWindow *window;
  GtkUIManager *ui;
  GtkNotebook *notebook;
  VteTerminal *active_term;
  gchar *title;
};

static void metaterm_app_create_new_tab (MetatermApp *self);

G_DEFINE_TYPE (MetatermApp, metaterm_app, GTK_TYPE_APPLICATION);

static void
file_new_tab_callback (GtkAction  *action,
                       gpointer    datap)
{
  MetatermApp *app = METATERM_APP (datap);

  metaterm_app_create_new_tab (app);
}

static void
file_close_tab_callback (GtkAction  *action,
                         gpointer    datap)
{
  MetatermApp *app = METATERM_APP (datap);

  gtk_notebook_remove_page (app->priv->notebook,
                            gtk_notebook_get_current_page (app->priv->notebook));
}

static void
edit_copy_callback (GtkAction      *action,
                    gpointer        datap)
{
  MetatermApp *app = datap;

  vte_terminal_copy_clipboard (app->priv->active_term);
}

static void
clipboard_targets_received_cb (GtkClipboard *clipboard,
                               GdkAtom *targets,
                               int n_targets,
                               gpointer data)
{
  VteTerminal *term = data;

  if (targets)
    vte_terminal_paste_clipboard (term);

  g_object_unref (term);
}

static void
edit_paste_callback (GtkAction    *action,
                     gpointer      datap)
{
  MetatermApp *app = datap;
  GtkClipboard *clipboard;

  if (!app->priv->active_term)
    return;
      
  clipboard = gtk_widget_get_clipboard (GTK_WIDGET (app->priv->window), GDK_SELECTION_CLIPBOARD);

  gtk_clipboard_request_targets (clipboard,
                                 (GtkClipboardTargetsReceivedFunc) clipboard_targets_received_cb,
                                 g_object_ref (app->priv->active_term));
}

static void
on_switch_page (GtkNotebook   *notebook,
                GtkWidget     *widget,
                guint          cur_page,
                gpointer       datap)
{
  MetatermApp *app = datap;

  app->priv->active_term = (VteTerminal*)widget;
}

static void
file_quit_callback (GtkAction    *action,
                    gpointer      datap)
{
  MetatermApp *app = datap;

  gtk_widget_destroy (GTK_WIDGET (app->priv->window));
}

static void
metaterm_app_base_init (MetatermApp *self)
{
  GtkAccelGroup *accel_group;
  GError *error = NULL;
  GtkWidget *menubar;
  GtkWidget *main_vbox;
  const GtkActionEntry menu_entries[] =
    {
      /* Toplevel */
      { "File", NULL, N_("File") },
      { "Edit", NULL, N_("Edit") },

      /* File menu */
      { "FileNewTab", GTK_STOCK_NEW, N_("Open Tab"), "<shift><control>T",
        NULL,
        G_CALLBACK (file_new_tab_callback) },
      { "FileCloseTab", GTK_STOCK_CLOSE, N_("Close Tab"), "<shift><control>W",
        NULL,
        G_CALLBACK (file_close_tab_callback) },
      { "FileQuit", GTK_STOCK_QUIT, N_("Quit"), "<shift><control>Q",
        NULL,
        G_CALLBACK (file_quit_callback) },

      /* Edit menu */
      { "EditCopy", GTK_STOCK_COPY, NULL, "<shift><control>C",
        NULL,
        G_CALLBACK (edit_copy_callback) },
      { "EditPaste", GTK_STOCK_PASTE, NULL, "<shift><control>V",
        NULL,
        G_CALLBACK (edit_paste_callback) },
    };

  if (self->priv->window)
    return;

  self->priv->window = GTK_WINDOW (gtk_window_new (GTK_WINDOW_TOPLEVEL));
  if (self->priv->title)
    gtk_window_set_title (self->priv->window, self->priv->title);

  main_vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (self->priv->window), main_vbox);
  gtk_widget_show (main_vbox);

  self->priv->ui = gtk_ui_manager_new ();
  accel_group = gtk_ui_manager_get_accel_group (self->priv->ui);
  gtk_window_add_accel_group (GTK_WINDOW (self->priv->window), accel_group);

  self->priv->actions = gtk_action_group_new ("Main");
  gtk_action_group_add_actions (self->priv->actions, menu_entries,
                                G_N_ELEMENTS (menu_entries), self);

  gtk_ui_manager_insert_action_group (self->priv->ui, self->priv->actions, 0);
  g_object_unref (self->priv->actions);

  gtk_ui_manager_add_ui_from_string (self->priv->ui, "<ui>"
" <menubar>"
"   <menu action=\"File\">"
"     <menuitem action=\"FileNewTab\" />"
"     <separator />"
"     <menuitem action=\"FileCloseTab\" />"
"     <menuitem action=\"FileQuit\" />"
"   </menu>"
"   <menu action=\"Edit\">"
"     <menuitem action=\"EditCopy\" />"
"     <menuitem action=\"EditPaste\" />"
"   </menu>"
" </menubar>"
"</ui>"
                                     , -1, &error);
  g_assert_no_error (error);

  menubar = gtk_ui_manager_get_widget (self->priv->ui, "/menubar");
  gtk_box_pack_start (GTK_BOX (main_vbox),
		      menubar,
		      FALSE, FALSE, 0);

  gtk_window_set_default_size (GTK_WINDOW (self->priv->window), 640, 480);
  self->priv->notebook = GTK_NOTEBOOK (gtk_notebook_new ());
  g_signal_connect (self->priv->notebook, "switch-page",
                    G_CALLBACK (on_switch_page), self);
  gtk_box_pack_end (GTK_BOX (main_vbox), GTK_WIDGET (self->priv->notebook), TRUE, TRUE, 0);
  gtk_application_add_window (GTK_APPLICATION (self), self->priv->window);
  gtk_widget_show_all (GTK_WIDGET (self->priv->window));
}

static void
metaterm_app_create_new_tab (MetatermApp *self)
{
  GtkLabel *label;
  VteTerminal *term;

  metaterm_app_base_init (self);

  term = (VteTerminal*)vte_terminal_new ();
  gtk_widget_show_all (GTK_WIDGET (term));
  label = GTK_LABEL (gtk_label_new (_("Tab")));
  gtk_widget_show_all (GTK_WIDGET (label));
  gtk_notebook_insert_page (self->priv->notebook,
                            GTK_WIDGET (term),
                            GTK_WIDGET (label),
                            0);
  gtk_widget_grab_focus (GTK_WIDGET (term));

  METATERM_APP_GET_CLASS (self)->new_tab (self, label, term);
}

static void
metaterm_app_default_new_tab (MetatermApp *self, GtkLabel *label, VteTerminal *term)
{
  char *child_argv[] = { "/bin/sh", NULL };

  gtk_label_set_text (label, "sh");

  vte_terminal_fork_command_full (term,
                                  VTE_PTY_DEFAULT,
                                  NULL,
                                  child_argv,
                                  NULL,
                                  0,
                                  NULL,
                                  NULL,
                                  NULL,
                                  NULL);
}

static void
metaterm_app_do_activate (MetatermApp *self)
{
  metaterm_app_create_new_tab (self);
}

static void
metaterm_app_init (MetatermApp *self)
{
  self->priv = G_TYPE_INSTANCE_GET_PRIVATE (self, METATERM_TYPE_APP,
					    MetatermAppPrivate);
  g_signal_connect (self, "activate", G_CALLBACK (metaterm_app_do_activate), NULL);
}

static void
metaterm_app_finalize (GObject *object)
{
  MetatermApp *self = METATERM_APP (object);

  (* G_OBJECT_CLASS (metaterm_app_parent_class)->finalize) (G_OBJECT (self));
}

static void
metaterm_app_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  MetatermApp *app = METATERM_APP (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, app->priv->title);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
metaterm_app_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  MetatermApp *self = METATERM_APP (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      self->priv->title = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
metaterm_app_class_init (MetatermAppClass *class)
{
  GObjectClass *gobject_class;
  MetatermAppClass *app_class;

  gobject_class = G_OBJECT_CLASS (class);
  app_class = METATERM_APP_CLASS (class);

  gobject_class->finalize = metaterm_app_finalize;
  gobject_class->get_property = metaterm_app_get_property;
  gobject_class->set_property = metaterm_app_set_property;

  app_class->new_tab = metaterm_app_default_new_tab;

  g_object_class_install_property (gobject_class, PROP_TITLE,
    g_param_spec_string ("title", "", "",
                         NULL, G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS));

  g_type_class_add_private (class, sizeof (MetatermAppPrivate));
}

/**
 * metaterm_app_get_window:
 * @app:
 *
 * Returns: (transfer none): The window for this app
 */
GtkWindow *
metaterm_app_get_window (MetatermApp *self)
{
  return self->priv->window;
}

void
metaterm_app_run (MetatermApp *self)
{
  gtk_widget_show (GTK_WIDGET (self->priv->window));
}
