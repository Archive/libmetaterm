/* 
 * Copyright (C) 2011 Colin Walters <walters@verbum.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __METATERM_H__
#define __METATERM_H__

#include <gtk/gtk.h>
#include <vte/vte.h>

G_BEGIN_DECLS

#define METATERM_TYPE_APP              (metaterm_app_get_type ())
#define METATERM_APP(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), METATERM_TYPE_APP, MetatermApp))
#define METATERM_APP_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), METATERM_TYPE_APP, MetatermAppClass))
#define METATERM_IS_APP(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), METATERM_TYPE_APP))
#define METATERM_IS_APP_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), METATERM_TYPE_APP))
#define METATERM_APP_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), METATERM_TYPE_APP, MetatermAppClass))

typedef struct _MetatermApp         MetatermApp;
typedef struct _MetatermAppClass    MetatermAppClass;
typedef struct _MetatermAppPrivate  MetatermAppPrivate;

struct _MetatermApp
{
  GtkApplication parent;

  /*< private >*/
  MetatermAppPrivate *priv;
};

struct _MetatermAppClass
{
  GtkApplicationClass parent;

  void (*new_tab) (MetatermApp *app, GtkLabel *label, VteTerminal *terminal);
};

GType metaterm_app_get_type (void);

MetatermApp * metaterm_app_new (void);

void metaterm_app_set_default_fork_command (MetatermApp *app,
					    const char **argv);

void metaterm_app_run (MetatermApp *app);

G_END_DECLS

#endif  /* __METATERM_H__ */

